# -*- coding: utf-8 -*-
import sys


def print_python_version():
    """Print the Python version
    """
    print(f"{sys.version}")

if __name__ == '__main__':
    print_python_version()
