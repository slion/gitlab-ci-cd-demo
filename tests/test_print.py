# -*- coding: utf-8 -*-

from unittest.mock import patch

from demo_app.app import print_python_version


@patch("builtins.print")
def test_print_python_version(mock_print):
    print_python_version()

    mock_print.assert_called_once()
